const get = id => ({ or: fallback => document.getElementById(id).value.trim() || fallback });
const set = id => ({ to: it => document.getElementById(id).value = it });
const safe = text => encodeURIComponent(text);

const generate = () => {
  const baseUrl = get('base_url').or('https://example.com');
  const summary = get('summary').or('SUMMARY_MISSING');
  const description = get('description').or('DESCRIPTION_MISSING');

  const destUrl = `${baseUrl}?summary=${safe(summary)}&description=${safe(description)}`;

  console.debug('jira-former.js', {
    function: 'generate',
    baseUrl,
    summary,
    description,
    destUrl,
  });

  set('result').to(destUrl);
  navigator.clipboard.writeText(destUrl);
};

const clipDest = () => {
  const destUrl = get('result').or(undefined);
  destUrl && navigator.clipboard.writeText(destUrl);
};

const openDest = () => {
  const destUrl = get('result').or(undefined);
  const child = destUrl && window.open(destUrl, '_blank');
  child && child.focus();
};
