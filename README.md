# Jira Former

The site is at: https://booniepepper.gitlab.io/jira-former/

It's a project for partially filling out Jira forms.

Currently it only supports pre-filling Subject and Description
which are pretty universal.

Technical details on how Jira processes URLs can be found here: https://community.atlassian.com/t5/Jira-Service-Management-articles/Pre-populate-Fields-from-URL/ba-p/1997476

## Usage instructions

Fill out the top information with placeholder stuff, click "generate",
and then put that link wherever you would have described how to fill it out.

# Other

2024 side quest of [J.R. Hill](https://so.dang.cool)

Please use the site for free, and share with anyone who might like it. You
may also freely view and use the source code under the terms of the
[3-Clause BSD License](https://opensource.org/license/bsd-3-clause/).
